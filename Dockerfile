FROM node:14-alpine AS BUILDER

WORKDIR /app
COPY package.json yarn.lock /app/
RUN yarn install
COPY . /app
RUN yarn build

FROM nginx:latest
COPY deployment/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/build  /usr/share/nginx/html
