import React from 'react';

export type StepsModalProps = {
  open: boolean;
  onClose: () => void;
  onNext: () => void;
  onPrevious: () => void;
  title?: string;
  onSave?: () => void;
  useCancelButton?: boolean;
  isLoading?: boolean;
  leftActions?: React.ReactNode;
  showSaveButton?: boolean;
  showPreviousButton?: boolean;
  saveButtonDisabled?: boolean;
};
