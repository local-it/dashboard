import React from 'react';

export type ModalProps = {
  open: boolean;
  onClose: () => void;
  title?: string;
  onSave?: () => void;
  saveButtonTitle?: string;
  useCancelButton?: boolean;
  cancelButtonTitle?: string;
  isLoading?: boolean;
  leftActions?: React.ReactNode;
  saveButtonDisabled?: boolean;
};
