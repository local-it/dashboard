type Tab = {
  disabled?: boolean;
  name: string;
  component: JSX.Element;
};

export interface TabsProps {
  tabs: Tab[];
  onTabClick?: (index: number) => void;
}

export interface TabPanelProps {
  isActive: boolean;
  children: JSX.Element | JSX.Element[];
}

export interface TabProps {
  title: string;
  isActive: boolean;
  onPress(): void;
  disabled: boolean;
}
