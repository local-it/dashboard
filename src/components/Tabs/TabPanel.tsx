import React from 'react';
import { TabPanelProps } from './types';

export const TabPanel = ({ children, isActive }: TabPanelProps) => {
  if (!isActive) return null;

  return <div className="pt-8 pb-4">{children}</div>;
};
