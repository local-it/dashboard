export type UserModalProps = {
  open: boolean;
  onClose: () => void;
  userId: string | null;
  setUserId: any;
};
