import { ApiStatus } from 'src/services/api/redux/types';

import { Auth } from '../types';

export interface AuthState extends Auth {
  _status: ApiStatus;
}
