import { User } from '../users';

export interface Auth {
  token: string | null;
  userInfo: User | null;
}
