import { State } from 'src/redux';

export const getUsers = (state: State) => state.users.users;
export const getUserById = (state: State) => state.users.user;
export const getUserModalLoading = (state: State) => state.users.userModalLoading;
export const getUserslLoading = (state: State) => state.users.usersLoading;
