import { ApiStatus } from 'src/services/api/redux';

import { User } from '../types';

export interface CurrentUserState extends User {
  _status: ApiStatus;
}

export interface UsersState {
  currentUser: CurrentUserState;
  users: User[];
  user: User;
  userModalLoading: boolean;
  usersLoading: boolean;
}

export interface CurrentUserUpdateAPI {
  id: number;
  phoneNumber?: string;
  email?: string;
  language?: string;
  country?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
}
