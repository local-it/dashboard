export interface StartAction {
  type: string;
  payload: any;
}

export interface SuccessAction {
  type: string;
  payload: any;
}

export interface FailureAction {
  type: string;
  payload: { error: string };
}

export interface ApiStatus {
  isEmpty: boolean;
  isLoading: boolean;
  isValid: boolean;
  isError: boolean;
  dateTime: number;
  error: any;
  errorMessage: string | null;
}

export interface ApiState<T = any> {
  _status: ApiStatus;
  items: T[];
}
