import { App, AppStatus, AppStatusEnum } from './types';

const transformAppStatus = (status: AppStatus) => {
  if (status.installed && status.ready) return AppStatusEnum.Installed;
  if (status.installed && !status.ready) return AppStatusEnum.Installing;
  return AppStatusEnum.NotInstalled;
};

export const transformApp = (response: any): App => {
  return {
    id: response.id ?? '',
    name: response.name ?? '',
    slug: response.slug ?? '',
    status: transformAppStatus(response.status),
    url: response.url,
    automaticUpdates: response.automatic_updates,
    assetSrc: `/assets/${response.slug}.svg`,
    markdownSrc: `/markdown/${response.slug}.md`,
  };
};

export const transformAppRequest = (data: App) => {
  return {
    automatic_updates: data.automaticUpdates,
    configuration: data.configuration,
  };
};

export const transformInstallAppRequest = (data: App) => {
  return {
    url: data.url,
    configuration: data.configuration,
  };
};
