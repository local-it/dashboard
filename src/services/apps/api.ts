import { performApiCall } from '../api';
import { transformApp } from './transformations';
import { App } from './types';

export const fetchApps = async (): Promise<App> => {
  // @ts-ignore
  const apiUrl = window.env.REACT_APP_API_URL;

  const res = await performApiCall({ hostname: apiUrl, path: '/apps', method: 'GET' });
  return transformApp(res);
};
