import { State } from 'src/redux';

export const getApps = (state: State) => state.apps.apps;
export const getCurrentApp = (state: State) => state.apps.currentApp;
export const getAppLoading = (state: State) => state.apps.appLoading;
export const getAppsLoading = (state: State) => state.apps.appsLoading;
