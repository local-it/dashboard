import { ApiStatus } from 'src/services/api/redux';

import { App } from '../types';

export interface CurrentUserState extends App {
  _status: ApiStatus;
}

export interface AppsState {
  currentApp: CurrentUserState;
  apps: App[];
  appLoading: boolean;
  appsLoading: boolean;
}
