export const DEFAULT_API_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

export const DEFAULT_API_DATE_FORMAT = 'yyyy-MM-dd';

export const DEFAULT_DATE_FORMAT = 'dd/MM/yyyy HH:mm';

export const MAX_TABLE_ROWS = 15;

export const VALIDATION_MESSAGES = {
  required: (name: string) => `${name} cannot be blank`,
  invalidEmail: () => 'Email address is invalid',
  passwordConfirmation: () => 'Passwords do not match',
  min: (name: string, min: number) => `${name} must be at least ${min}`,
};

export const BACK_TO_PAGE = 'prevPage';
export const BACK_TO_SEARCH = 'prevSearch';
export const RETURN_TO = 'returnTo';
export const RICH_EDITOR_DEFAULT_TAB = 'richEditorTab';
