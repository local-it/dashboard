import { get } from 'lodash';
import { FormState } from 'react-hook-form';

export function isTouched<T>(formState: FormState<T>, fieldName: keyof T | string) {
  return formState.isSubmitted || get(formState.touchedFields, fieldName);
}
