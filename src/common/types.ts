export interface ImageApiData {
  data: {
    id: number;
    fileId: number;
    name: string;
    isMain: boolean;
    url: string;
    smallUrl: string;
    mediumUrl: string;
    originalFileId: number;
    mediumFileId: number;
    smallFileId: number;
  };
}

export interface ImageMediaData {
  id: number;
  fileId: number;
  name: string;
  isMain: boolean;
  url: string;
  smallUrl: string;
  mediumUrl: string;
  originalFileId: number;
  mediumFileId: number;
  smallFileId: number;
  smallFile?: {
    id: number;
    name: string;
    url: string;
  };
}

export type ImageUploaderValue = File | string | ImageMediaData;

export type Breadcrumb = {
  label: string;
  href: string;
};
