import { Store } from 'redux';

import { AuthState } from 'src/services/auth/redux';
import { UsersState } from 'src/services/users/redux';
import { AppsState } from 'src/services/apps/redux';

export interface AppStore extends Store, State {}

export interface State {
  auth: AuthState;
  users: UsersState;
  apps: AppsState;
}
