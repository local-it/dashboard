import React, { useState } from 'react';
import { ChevronDownIcon, ChevronRightIcon, ClipboardIcon, KeyIcon } from '@heroicons/react/outline';
import { showToast } from 'src/common/util/show-toast';
import { Table } from 'src/components';
import { ChangeSecretModal } from './ChangeSecretModal';

export const Secrets = () => {
  const [secretModal, setSecretModal] = useState(false);
  const [openDangerZone, setOpenDangerZone] = useState(false);

  const secretModalOpen = () => setSecretModal(true);
  const secretModalClose = () => setSecretModal(false);

  const secretsModalSave = () => {
    showToast('The secret has been saved successfully.');
    setSecretModal(false);
  };

  const toggleDangerZone = () => setOpenDangerZone((prevState) => !prevState);

  const columns: any = React.useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
        width: '25%',
      },
      {
        Header: 'Secret',
        accessor: 'secret',
        Cell: (e: any) => {
          const [reveal, setReveal] = useState(false);

          return (
            <div className="flex items-center">
              {!reveal ? (
                <>
                  <div className="w-48 bg-gray-200 h-4 rounded-full mr-4" />
                  <button
                    onClick={() => setReveal(true)}
                    type="button"
                    className="inline-flex items-center px-2.5 py-1.5  text-xs font-medium rounded text-gray-700 hover:bg-gray-200 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
                  >
                    Reveal secret
                  </button>
                </>
              ) : (
                <div>{e.cell.row.original.secret}</div>
              )}
            </div>
          );
        },
      },
      {
        Header: ' ',
        Cell: () => {
          return (
            <div className="flex items-center justify-end opacity-0 group-hover:opacity-100 transition-opacity">
              <button
                onClick={() => showToast('The secret has been copied to your clipboard.')}
                type="button"
                className="inline-flex items-center px-4 py-2 border border-gray-200 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 mr-3"
              >
                <ClipboardIcon className="-ml-0.5 mr-2 h-4 w-4" aria-hidden="true" />
                Copy Secret
              </button>

              <button
                onClick={secretModalOpen}
                type="button"
                className="inline-flex items-center px-4 py-2 border border-red-400 shadow-sm text-sm font-medium rounded-md text-red-700 bg-red-50 hover:bg-red-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
              >
                <KeyIcon className="-ml-0.5 mr-2 h-4 w-4" aria-hidden="true" />
                Change secret
              </button>
            </div>
          );
        },
      },
    ],
    [],
  );

  const data: any[] = React.useMemo(
    () => [
      {
        id: 1,
        name: 'nextcloud_mariadb_password',
        secret: 'xEtC2dvUsBVzoTA19fOyDF4IwpPSWW62I92F59s69EO9vp56',
      },
      {
        id: 2,
        name: 'nextcloud_mariadb_root_password',
        secret: `OB.IHD:HPY{x<)>@+B'p(LUU@k-Wv=w(!@)?7Zq%@nE7GY]!`,
      },
      {
        id: 3,
        name: 'nextcloud_password (admin login)',
        secret: `#B&>4A;O#XEF]_dE*zxF26>8fF:akgGL8gi#yALK{ZY[P$eE`,
      },
    ],
    [],
  );

  return (
    <>
      <div
        className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between mt-24 mb-5 cursor-pointer"
        onClick={toggleDangerZone}
      >
        <h1 className="text-2xl leading-6 font-medium text-gray-900">Danger zone</h1>

        <button
          type="button"
          className="inline-flex items-center px-2 py-2 text-sm font-medium rounded-md text-gray-900 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
        >
          {openDangerZone ? (
            <ChevronDownIcon className="flex-shrink-0 h-5 w-5 text-gray-600" aria-hidden="true" />
          ) : (
            <ChevronRightIcon className="flex-shrink-0 h-5 w-5 text-gray-600" aria-hidden="true" />
          )}
        </button>
      </div>

      {openDangerZone && (
        <div className="border-2 border-red-500 rounded-md overflow-hidden">
          <div className="border border-gray-200">
            <Table data={data} columns={columns} />
          </div>
        </div>
      )}

      <ChangeSecretModal open={secretModal} onClose={secretModalClose} onSave={secretsModalSave} />
    </>
  );
};
