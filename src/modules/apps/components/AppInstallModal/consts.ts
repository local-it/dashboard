import { App } from 'src/services/apps';

export const initialCode = `luck: except
natural: still
near: though
search:
  - feature
  - - 1980732354.689713
    - hour
    - butter:
        ordinary: 995901949.8974948
        teeth: true
        whole:
          - -952367353
          - - talk: -1773961379
              temperature: false
              oxygen: true
              laugh:
                flag:
                  in: 2144751662
                  hospital: -1544066384.1973226
                  law: congress
                  great: stomach`;

export const initialAppForm = {
  url: '',
} as App;
