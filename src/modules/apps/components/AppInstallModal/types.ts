export type AppInstallModalProps = {
  open: boolean;
  onClose: () => void;
  appSlug: string | null;
};
