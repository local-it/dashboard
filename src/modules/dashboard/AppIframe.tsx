import React from 'react';
import Iframe from 'react-iframe';

export const AppIframe: React.FC<any> = ({ app }: { app: any }) => {
  return (
    <div className="relative">
      <div style={{ minHeight: '95vh' }}>
        <Iframe
          height="100%"
          width="100%"
          position="absolute"
          frameBorder={0}
          overflow="hidden"
          title={app.name}
          url={app.url}
        />
      </div>
    </div>
  );
};
