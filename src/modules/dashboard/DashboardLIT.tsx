import React, { useEffect } from 'react';
import { AppStatusEnum, useApps } from 'src/services/apps';
import { useAuth } from 'src/services/auth';
import { DashboardUtility } from './components';
import { DashboardCard } from './components/DashboardCard/DashboardCardLIT';
import { UTILITY_APPS } from './consts';

export const Dashboard: React.FC = () => {
  const { apps, loadApps, appTableLoading } = useApps();
  const { isAdmin } = useAuth();

  // Tell React to load the apps
  useEffect(() => {
    loadApps();

    return () => {};
  }, []);

  return (
    <div className="relative">
      <div className="max-w-7xl mx-auto py-4 px-3 sm:px-6 lg:px-8">
        <div className="mt-6 pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
          <h1 className="text-3xl leading-6 font-bold text-gray-900">Dashboard</h1>
        </div>
      </div>
      <div className="max-w-7xl mx-auto py-4 px-3 sm:px-6 lg:px-8 h-full flex-grow">
        <div className="grid grid-cols-1 md:grid-cols-2 md:gap-4 lg:grid-cols-4 mb-10">
          {!appTableLoading &&
            apps
              .filter((app) => UTILITY_APPS.indexOf(app.slug) === -1)
              .map((app) => <DashboardCard app={app} key={app.name} />)}
        </div>
      </div>
      {isAdmin && (
        <div className="max-w-4xl mx-auto py-4 sm:px-6 lg:px-8 h-full flex-grow">
          <div className="pb-4 border-b border-gray-200 sm:flex sm:items-center">
            <h3 className="text-lg leading-6 font-medium text-gray-900">Administration</h3>
          </div>

          <dl className="mt-5 grid grid-cols-1 gap-2 sm:grid-cols-2">
            {apps
              .filter((app) => UTILITY_APPS.indexOf(app.slug) !== -1 && app.url !== null)
              .map((app) => (
                <DashboardUtility item={app} key={app.name} />
              ))}
          </dl>
        </div>
      )}
    </div>
  );
};
