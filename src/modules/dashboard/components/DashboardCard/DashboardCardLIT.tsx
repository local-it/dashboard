import React from 'react';
import { Link } from 'react-router-dom';

export const DashboardCard: React.FC<any> = ({ app }: { app: any }) => {
  return (
    <>
      <Link
        to={`/${app.slug}`}
        // className="mx-1 inline-flex items-center px-2.5 py-1.5 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
      >
        <div
          className="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-100 mb-4 md:mb-0"
          key={app.name}
        >
          <div className="px-4 py-5 sm:p-4">
            <div className="mr-4 flex items-center">
              <img
                className="h-16 w-16 rounded-md overflow-hidden mr-4 flex-shrink-0"
                src={app.assetSrc}
                alt={app.name}
              />

              <div>
                <h2 className="text-xl litbutton-card leading-8 font-bold">{app.name}</h2>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </>
  );
};
