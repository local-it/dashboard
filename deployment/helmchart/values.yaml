## @section Global parameters
## Global container image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global container image parameters: imageRegistry, imagePullSecrets and storageClass

## @param global.imageRegistry Global container image registry
## @param global.imagePullSecrets Global container registry secret names as an array
## @param global.storageClass Global StorageClass for Persistent Volume(s)
##
global:
  imageRegistry: ""
  ## E.g.
  ## imagePullSecrets:
  ##   - myRegistryKeySecretName
  ##
  imagePullSecrets: []
  storageClass: ""

## @section Common parameters

## @param kubeVersion Force target Kubernetes version (using Helm capabilities if not set)
##
kubeVersion: ""
## @param nameOverride String to partially override dashboard.fullname template (will maintain the release name)
##
nameOverride: ""
## @param fullnameOverride String to fully override dashboard.fullname template
##
fullnameOverride: ""
## @param commonLabels Labels to be added to all deployed resources
##
commonLabels: {}
## @param commonAnnotations Annotations to be added to all deployed resources
##
commonAnnotations: {}



## @section Stackspin parameters

## @param imagePullSecrets Specify docker-registry secret names as an array
## @param imagePullPolicy  Policy on pulling images on container start
##
image:
  imagePullSecrets: []

  ## Specify a imagePullPolicy
  ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
  ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
  ##
  ## Same policy for all containers is used.
  ##
  pullPolicy: IfNotPresent

## Dashboard configuration parameters
## ref: https://open.greenhost.net/stackspin/dashboard
##
dashboard:
  ## Dashboard image version
  ## ref: https://hub.docker.com/r/bitnami/dashboard/tags/
  ## @param image.registry Stackspin image registry
  ## @param image.repository Stackspin image repository
  ## @param image.tag Stackspin image tag
  ## @param image.pullPolicy Stackspin image pull policy
  ## @param image.pullSecrets Stackspin image pull secrets
  ## @param image.debug Specify if debug logs should be enabled
  ##
  image:
    registry: open.greenhost.net:4567
    repository: stackspin/dashboard/dashboard
    tag: 0.5.1
    digest: ""
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ## e.g:
    ## pullSecrets:
    ##   - myRegistryKeySecretName
    ##
    pullSecrets: []
  ## @section Service parameters

  ## Kubernetes service configuration. For minikube, set this to NodePort, elsewhere use LoadBalancer or ClusterIP
  ##
  service:
    ## @param service.type Kubernetes Service type
    ##
    type: ClusterIP
    ## @param service.port Service HTTP port
    ##
    port: 80
    ## @param service.nodePort Node Ports to expose
    ##
    nodePort: ""
    ## @param service.loadBalancerIP Use loadBalancerIP to request a specific static IP
    ##
    loadBalancerIP: ""
    ## @param service.externalTrafficPolicy Enable client source IP preservation
    ## ref http://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
    ##
    externalTrafficPolicy: Cluster
    ## @param service.annotations Service annotations
    ##
    annotations: {}
    ## @param service.loadBalancerSourceRanges Limits which cidr blocks can connect to service's load balancer
    ## Only valid if service.type: LoadBalancer
    ##
    loadBalancerSourceRanges: []
    ## @param service.extraPorts Extra ports to expose (normally used with the `sidecar` value)
    ##
    extraPorts: []
    ## @param service.nodePorts.http Kubernetes http node port
    ## Example:
    ##  nodePorts:
    ##   http: <to set explicitly, choose port between 30000-32767>
    ##
    nodePorts:
      http: ""
  ## @param dashboard.extendEslint Whether to read .eslint.json rules
  extendEslint: false
  ## @param dashboard.host Stackspin host to create application URLs (include the port if =/= 80)
  ##
  host: ""
  ## @param dashboard.command Custom command to override image cmd
  ##
  command: []
  ## @param dashboard.args Custom args for the custom command
  ##
  args: []
  ## @param dashboard.containerSecurityContext Container security context specification
  ## Example:
  ## capabilities:
  ##   drop:
  ##   - ALL
  ## readOnlyRootFilesystem: true
  ## runAsNonRoot: true
  ## runAsUser: 1000
  ##
  containerSecurityContext: {}
  ## Stackspin container's resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ## We usually recommend not to specify default resources and to leave this as a conscious
  ## choice for the user. This also increases chances charts run on environments with little
  ## resources, such as Minikube. If you do want to specify resources, uncomment the following
  ## lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  ## @param dashboard.resources.limits The resources limits for the container
  ## @param dashboard.resources.requests The requested resources for the container
  ##
  resources:
    ## Example:
    ## limits:
    ##    cpu: 100m
    ##    memory: 128Mi
    limits: {}
    ## Examples:
    ## requests:
    ##    cpu: 100m
    ##    memory: 128Mi
    requests: {}
  ## Stackspin extra options for liveness probe
  ## WARNING: Stackspin installation process may take up some time and
  ## setting inappropriate values here may lead to pods failure.
  ## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
  ## @param dashboard.livenessProbe.enabled Enable/disable livenessProbe
  ## @param dashboard.livenessProbe.initialDelaySeconds Delay before liveness probe is initiated
  ## @param dashboard.livenessProbe.periodSeconds How often to perform the probe
  ## @param dashboard.livenessProbe.timeoutSeconds When the probe times out
  ## @param dashboard.livenessProbe.failureThreshold Minimum consecutive failures for the probe
  ## @param dashboard.livenessProbe.successThreshold Minimum consecutive successes for the probe
  ##
  livenessProbe:
    enabled: true
    initialDelaySeconds: 10
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## Stackspin extra options for readiness probe
  ## WARNING: Stackspin installation process may take up some time and
  ## setting inappropriate values here may lead to pods failure.
  ## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
  ## @param dashboard.readinessProbe.enabled Enable/disable readinessProbe
  ## @param dashboard.readinessProbe.initialDelaySeconds Delay before readiness probe is initiated
  ## @param dashboard.readinessProbe.periodSeconds How often to perform the probe
  ## @param dashboard.readinessProbe.timeoutSeconds When the probe times out
  ## @param dashboard.readinessProbe.failureThreshold Minimum consecutive failures for the probe
  ## @param dashboard.readinessProbe.successThreshold Minimum consecutive successes for the probe
  ##
  readinessProbe:
    enabled: true
    initialDelaySeconds: 10
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## @param dashboard.customLivenessProbe Custom liveness probe to execute (when the main one is disabled)
  ##
  customLivenessProbe: {}
  ## @param dashboard.customReadinessProbe Custom readiness probe to execute (when the main one is disabled)
  ##
  customReadinessProbe: {}
  ## @param dashboard.extraEnvVars An array to add extra env vars
  ## For example:
  ## extraEnvVars:
  ##   dashboard:
  ##   - name: DASHBOARD_ELASTICSEARCH_URL
  ##     value: test
  ##
  extraEnvVars: []
  ## @param dashboard.extraEnvVarsCM Array to add extra configmaps
  ##
  extraEnvVarsCM: []
  ## @param dashboard.extraEnvVarsSecret Array to add extra environment variables from a secret
  ##
  extraEnvVarsSecret: ""
  ## @param dashboard.extraVolumeMounts Additional volume mounts (used along with `extraVolumes`)
  ## Example: Mount CA file
  ## extraVolumeMounts
  ##   - name: ca-cert
  ##     subPath: ca_cert
  ##     mountPath: /path/to/ca_cert
  ##
  extraVolumeMounts: []
## Dashboard backend configuration parameters
## ref: https://open.greenhost.net/stackspin/dashboard-backend
##
backend:
  ## Dashboard image version
  ## ref: https://hub.docker.com/r/bitnami/dashboard/tags/
  ## @param image.registry Stackspin image registry
  ## @param image.repository Stackspin image repository
  ## @param image.tag Stackspin image tag
  ## @param image.pullSecrets Stackspin image pull secrets
  ## @param image.debug Specify if debug logs should be enabled
  ##
  image:
    registry: open.greenhost.net:4567
    repository: stackspin/dashboard/dashboard-backend
    tag: 0.5.1
    digest: ""
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ## e.g:
    ## pullSecrets:
    ##   - myRegistryKeySecretName
    ##
    pullSecrets: []

  oidc:
    clientId: dashboard
    clientSecret: ReplaceWithSecret
    baseUrl: https://sso.stackspin.example.org
    authorizationBaseUrl: https://sso.stackspin.example.org/oauth2/auth
    tokenUrl: https://sso.stackspin.example.org/oauth2/token

  # Where to find the Kratos ADMIN and PUBLIC url
  kratos:
    publicUrl: https://dashboard.stackspin.example.org/kratos
    adminUrl: http://kratos-admin:80

  # Where to find the Hydra ADMIN url
  hydra:
    adminUrl: http://hydra-admin:4445

  # Public URL of login panel
  loginPanelUrl: https://dashboard.stackspin.example.org/web/
  databaseUrl: mysql+pymysql://stackspin:stackspin@single-sign-on-database-mariadb/stackspin

  initialUser:
    email: admin@example.com
    # password: Set a pasword for the initial user!

  ## Kubernetes service configuration. For minikube, set this to NodePort, elsewhere use LoadBalancer or ClusterIP
  ##
  service:
    ## @param service.type Kubernetes Service type
    ##
    type: ClusterIP
    ## @param service.port Service HTTP port
    ##
    port: 80
    ## @param service.nodePort Node Ports to expose
    ##
    nodePort: ""
    ## @param service.loadBalancerIP Use loadBalancerIP to request a specific static IP
    ##
    loadBalancerIP: ""
    ## @param service.externalTrafficPolicy Enable client source IP preservation
    ## ref http://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
    ##
    externalTrafficPolicy: Cluster
    ## @param service.annotations Service annotations
    ##
    annotations: {}
    ## @param service.loadBalancerSourceRanges Limits which cidr blocks can connect to service's load balancer
    ## Only valid if service.type: LoadBalancer
    ##
    loadBalancerSourceRanges: []
    ## @param service.extraPorts Extra ports to expose (normally used with the `sidecar` value)
    ##
    extraPorts: []
    ## @param service.nodePorts.http Kubernetes http node port
    ## Example:
    ##  nodePorts:
    ##   http: <to set explicitly, choose port between 30000-32767>
    ##
    nodePorts:
      http: ""


  ## @param backend.reactAppApiUrl URL to the dashboard-backend API
  apiUrl: "/api/v1"

  ## Variables for future use {{{

  ## @param backend.username Admin user of the application
  ##
  username: user
  ## @param backend.password password. WARNING: Minimum length of 10 characters
  ## Defaults to a random 10-character alphanumeric string if not set
  ##
  password: ""
  ## @param backend.existingSecret Name of an existing secret containing the password (ignores previous password)
  ## The secret should contain the following key:
  ## backend-password
  ##
  existingSecret: ""
  ## @param backend.email Admin user email of the application
  ##
  email: user@example.com

  ## dashboard SMTP settings
  ## @param backend.smtp.enabled Enable/disable SMTP
  ## @param backend.smtp.host SMTP host name
  ## @param backend.smtp.port SMTP port number
  ## @param backend.smtp.user SMTP account user name
  ## @param backend.smtp.password SMTP account password
  ## @param backend.smtp.protocol SMTP protocol (Allowed values: tls, ssl)
  ## @param backend.smtp.auth SMTP authentication method
  ## @param backend.smtp.existingSecret Name of an existing Kubernetes secret. The secret must have the following key configured: `smtp-password`
  ##
  smtp:
    enabled: false
    host: ""
    port: ""
    user: ""
    password: ""
    protocol: ""
    auth: ""
    existingSecret: ""

  ## End variables for future use }}}

  ## @param backend.flaskEnv Overridable flask env for debugging
  flaskEnv: production

  ## @param backend.secretKey Flask secret for generating JWT tokens
  # secretKey: OVERRIDE_ME

  ## @param backend.command Custom command to override image cmd
  ##
  command: []
  ## @param backend.args Custom args for the custom command
  ##
  args: []
  ## @param backend.containerSecurityContext Container security context specification
  ## Example:
  ## capabilities:
  ##   drop:
  ##   - ALL
  ## readOnlyRootFilesystem: true
  ## runAsNonRoot: true
  ## runAsUser: 1000
  ##
  containerSecurityContext: {}
  ## Stackspin container's resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ## We usually recommend not to specify default resources and to leave this as a conscious
  ## choice for the user. This also increases chances charts run on environments with little
  ## resources, such as Minikube. If you do want to specify resources, uncomment the following
  ## lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  ## @param backend.resources.limits The resources limits for the container
  ## @param backend.resources.requests The requested resources for the container
  ##
  resources:
    ## Example:
    ## limits:
    ##    cpu: 100m
    ##    memory: 128Mi
    limits: {}
    ## Examples:
    ## requests:
    ##    cpu: 100m
    ##    memory: 128Mi
    requests: {}
  ## Stackspin extra options for liveness probe
  ## WARNING: Stackspin installation process may take up some time and
  ## setting inappropriate values here may lead to pods failure.
  ## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
  ## @param backend.livenessProbe.enabled Enable/disable livenessProbe
  ## @param backend.livenessProbe.initialDelaySeconds Delay before liveness probe is initiated
  ## @param backend.livenessProbe.periodSeconds How often to perform the probe
  ## @param backend.livenessProbe.timeoutSeconds When the probe times out
  ## @param backend.livenessProbe.failureThreshold Minimum consecutive failures for the probe
  ## @param backend.livenessProbe.successThreshold Minimum consecutive successes for the probe
  ##
  livenessProbe:
    enabled: true
    initialDelaySeconds: 10
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## Stackspin extra options for readiness probe
  ## WARNING: Stackspin installation process may take up some time and
  ## setting inappropriate values here may lead to pods failure.
  ## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
  ## @param backend.readinessProbe.enabled Enable/disable readinessProbe
  ## @param backend.readinessProbe.initialDelaySeconds Delay before readiness probe is initiated
  ## @param backend.readinessProbe.periodSeconds How often to perform the probe
  ## @param backend.readinessProbe.timeoutSeconds When the probe times out
  ## @param backend.readinessProbe.failureThreshold Minimum consecutive failures for the probe
  ## @param backend.readinessProbe.successThreshold Minimum consecutive successes for the probe
  ##
  readinessProbe:
    enabled: true
    initialDelaySeconds: 10
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## @param backend.customLivenessProbe Custom liveness probe to execute (when the main one is disabled)
  ##
  customLivenessProbe: {}
  ## @param backend.customReadinessProbe Custom readiness probe to execute (when the main one is disabled)
  ##
  customReadinessProbe: {}
  ## @param backend.extraEnvVars An array to add extra env vars
  ## For example:
  ## extraEnvVars:
  ##   backend:
  ##   - name: DASHBOARD_ELASTICSEARCH_URL
  ##     value: test
  ##
  extraEnvVars: []
  ## @param backend.extraEnvVarsCM Array to add extra configmaps
  ##
  extraEnvVarsCM: []
  ## @param backend.extraEnvVarsSecret Array to add extra environment variables from a secret
  ##
  extraEnvVarsSecret: ""
  ## @param backend.extraVolumeMounts Additional volume mounts (used along with `extraVolumes`)
  ## Example: Mount CA file
  ## extraVolumeMounts
  ##   - name: ca-cert
  ##     subPath: ca_cert
  ##     mountPath: /path/to/ca_cert
  ##
  extraVolumeMounts: []

## @param replicaCount Number of Stackspin & Sidekiq replicas
## (Note that you will need ReadWriteMany PVCs for this to work properly)
##
replicaCount: 1
## @param extraVolumes Array of extra volumes to be added deployment. Requires setting `extraVolumeMounts`
## Example: Add secret volume
## extraVolumes:
##  - name: ca-cert
##    secret:
##      secretName: ca-cert
##      items:
##        - key: ca-cert
##          path: ca_cert
##
extraVolumes: []
## @param sidecars Attach additional sidecar containers to the pod
## Example:
## sidecars:
##   - name: your-image-name
##     image: your-image
##     imagePullPolicy: Always
##     ports:
##       - name: portname
##         containerPort: 1234
##
sidecars: []
## @param initContainers Additional init containers to add to the pods
##
## e.g.
## initContainers:
##   - name: your-image-name
##     image: your-image
##     imagePullPolicy: Always
##     ports:
##       - name: portname
##       containerPort: 1234
##
initContainers: []
## @param serviceAccount.create Whether the service account should be created
## @param serviceAccount.annotations Annotations to add to the service account
## @param serviceAccount.name Name to be used for the service account
##
serviceAccount:
  create: false
  annotations: {}
  ## If not set and create is true, a name is generated using the fullname template
  ##
  name: ""
## @param podSecurityContext Pod security context specification
## Example:
##  fsGroup: 2000
##
##
podSecurityContext: {}
## @param hostAliases Add deployment host aliases
## https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/
##
hostAliases: []
## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  ## @param persistence.enabled Whether to enable persistence based on Persistent Volume Claims
  ##
  enabled: false
  ## @param persistence.storageClass dashboard & sidekiq data Persistent Volume Storage Class
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  storageClass: ""
  ## @param persistence.existingClaim Use a existing PVC which must be created manually before bound
  ##
  existingClaim: ""
  ## @param persistence.accessMode PVC Access Mode (RWO, ROX, RWX)
  ##
  accessMode: ReadWriteOnce
  ## @param persistence.size Size of the PVC to request
  ##
  size: 10Gi
  ## @param persistence.selector Selector to match an existing Persistent Volume (this value is evaluated as a template)
  ## selector:
  ##   matchLabels:
  ##     app: my-app
  selector: {}
## @param updateStrategy.type Update strategy type. Only really applicable for deployments with RWO PVs attached
## If replicas = 1, an update can get "stuck", as the previous pod remains attached to the
## PV, and the "incoming" pod can never start. Changing the strategy to "Recreate" will
## terminate the single previous pod, so that the new, incoming pod can attach to the PV
## Example:
## updateStrategy:
##  type: RollingUpdate
##  rollingUpdate:
##    maxSurge: 25%
##    maxUnavailable: 25%
updateStrategy:
  type: RollingUpdate
## @param podAnnotations Additional pod annotations
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
##
podAnnotations: {}
## @param podLabels Additional pod labels
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
##
podLabels: {}
## @param podAffinityPreset Pod affinity preset. Allowed values: soft, hard
## ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
##
podAffinityPreset: ""
## @param podAntiAffinityPreset Pod anti-affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
##
podAntiAffinityPreset: soft
## Node affinity preset
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity
## @param nodeAffinityPreset.type Node affinity preset type. Ignored if `affinity` is set. Allowed values: `soft` or `hard`
## @param nodeAffinityPreset.key Node label key to match Ignored if `affinity` is set.
## @param nodeAffinityPreset.values Node label values to match. Ignored if `affinity` is set.
##
nodeAffinityPreset:
  type: ""
  ## E.g.
  ## key: "kubernetes.io/e2e-az-name"
  ##
  key: ""
  ## E.g.
  ## values:
  ##   - e2e-az1
  ##   - e2e-az2
  ##
  values: []
## @param affinity Affinity for pod assignment
## Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
## Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
##
affinity: {}
## @param nodeSelector Node labels for pod assignment.
## Ref: https://kubernetes.io/docs/user-guide/node-selection/
##
nodeSelector: {}
## @param tolerations Tolerations for pod assignment.
## Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
##
tolerations: []


## @section Volume Permissions parameters (currently disabled because there is
## no persistence in the app)

## Init containers parameters:
## volumePermissions: Change the owner and group of the persistent volume mountpoint to runAsUser:fsGroup
## values from the securityContext section.
##
volumePermissions:
  ## @param volumePermissions.enabled Enable init container that changes volume permissions in the data directory (for cases where the default k8s `runAsUser` and `fsUser` values do not work)
  ##
  enabled: false
  ## Init containers' resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ## We usually recommend not to specify default resources and to leave this as a conscious
  ## choice for the user. This also increases chances charts run on environments with little
  ## resources, such as Minikube. If you do want to specify resources, uncomment the following
  ## lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  ## @param volumePermissions.resources.limits The resources limits for the init container
  ## @param volumePermissions.resources.requests The requested resources for the init container
  ##
  resources:
    ## Example:
    ## limits:
    ##    cpu: 100m
    ##    memory: 128Mi
    limits: {}
    ## Examples:
    ## requests:
    ##    cpu: 100m
    ##    memory: 128Mi
    requests: {}

## @section Ingress parameters

## Ingress parameters
##
ingress:
  ## @param ingress.enabled Enable ingress controller resource
  ##
  enabled: false
  ## @param ingress.certManager Add annotations for cert-manager
  ##
  certManager: false
  ## @param ingress.hostname Default host for the ingress resource
  ##
  hostname: dashboard.local
  ## @param ingress.apiVersion Force Ingress API version (automatically detected if not set)
  ##
  apiVersion: ""
  ## @param ingress.path Ingress path
  ##
  path: /
  ## @param ingress.pathType Ingress path type
  ##
  pathType: Prefix
  ## @param ingress.annotations Ingress annotations done as key:value pairs
  ## For a full list of possible ingress annotations, please see
  ## ref: https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md
  ##
  ## If certManager is set to true, annotation kubernetes.io/tls-acme: "true" will automatically be set
  ##
  annotations: {}
  ## @param ingress.tls Enable TLS configuration for the hostname defined at ingress.hostname parameter
  ## TLS certificates will be retrieved from a TLS secret with name: {{- printf "%s-tls" .Values.ingress.hostname }}
  ## You can use the ingress.secrets parameter to create this TLS secret or relay on cert-manager to create it
  ##
  tls: false
  ## @param ingress.extraTls The tls configuration for additional hostnames to be covered with this ingress record.
  ## see: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
  ## extraTls:
  ## - hosts:
  ##     - dashboard.local
  ##   secretName: dashboard.local-tls
  extraTls: []
  ## @param ingress.secrets If you're providing your own certificates, please use this to add the certificates as secrets
  ## key and certificate should start with -----BEGIN CERTIFICATE----- or
  ## -----BEGIN RSA PRIVATE KEY-----
  ##
  ## name should line up with a tlsSecret set further up
  ## If you're using cert-manager, this is unneeded, as it will create the secret for you if it is not set
  ##
  ## It is also possible to create and manage the certificates outside of this helm chart
  ## Please see README.md for more information
  ## Example:
  ## - name: dashboard.local-tls
  ##   key:
  ##   certificate:
  secrets: []

# The dashboard-backend needs access to certain Kubernetes APIs to be able to
# install and remove apps
rbac:
  ## @param backend.rbac.create Specifies whether RBAC resources should be created
  create: true

## ServiceAccount configuration for dashboard backend
##
serviceAccount:
  ## @param backend.serviceAccount.create Specifies whether a ServiceAccount should be created
  ##
  create: true
  ## @param backend.serviceAccount.name The name of the ServiceAccount to use.
  ## If not set and create is true, a name is generated using the common.names.fullname template
  ##
  name: ""
  ## @param backend.serviceAccount.automountServiceAccountToken Automount service account token for the dashboard backend service account
  ##
  automountServiceAccountToken: true
  ## @param backend.serviceAccount.annotations Annotations for service account. Evaluated as a template. Only used if `create` is `true`.
  ##
  annotations: {}
