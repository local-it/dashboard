
{{/*
Create the name of the service account to use
*/}}
{{- define "dashboard.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Return the proper container image registry secret names
*/}}
{{- define "dashboard.imagePullSecrets" -}}
{{ include "common.images.pullSecrets" (dict "images" (list .Values.image) "global" .Values.global) }}
{{- end -}}

{{/*
Return true if a secret object for backend should be created
*/}}
{{- define "backend.createSecret" -}}
{{- if or (not .Values.backend.existingSecret) (and (not .Values.backend.smtp.existingSecret) .Values.backend.smtp.password .Values.backend.smtp.enabled) }}
    {{- true -}}
{{- end -}}
{{- end -}}

{{/*
Return the backend secret name
*/}}
{{- define "backend.secretName" -}}
{{- if .Values.backend.existingSecret }}
    {{- printf "%s" .Values.backend.existingSecret -}}
{{- else -}}
    {{- printf "%s-backend" (include "common.names.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Return the backend SMTP secret name
*/}}
{{- define "backend.smtp.secretName" -}}
{{- if .Values.backend.smtp.existingSecret }}
    {{- printf "%s" .Values.backend.smtp.existingSecret -}}
{{- else -}}
    {{- printf "%s-backend" (include "common.names.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Return true if backend SMTP uses password authentication
*/}}
{{- define "backend.smtp.password.enabled" -}}
{{- if and (or .Values.backend.smtp.password .Values.backend.smtp.existingSecret) .Values.backend.smtp.enabled }}
    {{- true -}}
{{- end -}}
{{- end -}}

{{/*
Get the user defined LoadBalancerIP for this release
Note, returns 127.0.0.1 if using ClusterIP.
*/}}
{{- define "dashboard.serviceIP" -}}
{{- if eq .Values.dashboard.service.type "ClusterIP" -}}
127.0.0.1
{{- else -}}
{{- .Values.dashboard.service.loadBalancerIP | default "" -}}
{{- end -}}
{{- end -}}

{{/*
Gets the host to be used for this application.
If not using ClusterIP, or if a host or LoadBalancerIP is not defined, the value will be empty
*/}}
{{- define "dashboard.host" -}}
{{- $host := .Values.dashboard.host | default "" -}}
{{- default (include "dashboard.serviceIP" .) $host -}}
{{- end -}}

{{/*
Service name for the backend service
*/}}
{{- define "backend.serviceName" -}}
  {{ include "common.names.fullname" . }}-backend
{{- end -}}

{{/*
Service name for the dashboard service
*/}}
{{- define "dashboard.serviceName" -}}
  {{ include "common.names.fullname" . }}
{{- end -}}

{{/*
Get the user defined LoadBalancerIP for this release
Note, returns 127.0.0.1 if using ClusterIP.
*/}}
{{- define "backend.serviceIP" -}}
{{- if eq .Values.backend.service.type "ClusterIP" -}}
127.0.0.1
{{- else -}}
{{- .Values.backend.service.loadBalancerIP | default "" -}}
{{- end -}}
{{- end -}}

{{/*
Gets the host to be used for this application.
If not using ClusterIP, or if a host or LoadBalancerIP is not defined, the value will be empty
*/}}
{{- define "backend.host" -}}
{{- $host := .Values.backend.host | default "" -}}
{{- default (include "backend.serviceIP" .) $host -}}
{{- end -}}

{{/*
Return the proper dashboard image name
*/}}
{{- define "dashboard.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.dashboard.image "global" .Values.global) }}
{{- end -}}
{{/*
Return the proper backend image name
*/}}
{{- define "backend.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.backend.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Storage Class
*/}}
{{- define "dashboard.storageClass" -}}
{{- include "common.storage.class" (dict "persistence" .Values.persistence "global" .Values.global) -}}
{{- end -}}
