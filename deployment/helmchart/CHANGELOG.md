# Changelog

## [1.5.1]

* Update dashboard to version 0.5.1

## [1.5.0]

* Use dashboard backend container from new location
* Use proper semver image tags

## [1.4.0]

* Fetch apps from Kubernetes back-end
* Also fetch external apps
* Only show installed apps on dashboard
* Use correct URLs for apps on dashboard (not hardcoded ones)

## [1.2.3]

* Fix logging out of Hydra from the dashboard

## [1.2.2]

### Bug fixes

* Fix invalid reference format when image.digest is missing
  see https://open.greenhost.net/stackspin/stackspin/-/merge_requests/1397#note_50785

## [1.2.0]

### Features

* Batch user creation by pasting CSV in the dashboard
* When an admin's dashboard access is changed to "User", their app access now
  defaults to "user"

## [1.1.0]

### Bug fixes

* Logging out of dashboard now calls SSO signout URL based on current domain

### Features

* Dashboard admin users automatically have admin rights in all apps
* App-specific rights for dashboard admin users are not editable

## [1.0.5]

### Bug fixes

* Propagate admin access to all apps for dashboard admins #62

### Features

* Update dashboard to v0.2.6
* Update dashboard-backend to v0.2.7

## [1.0.4]

### Bug fixes

* Don't fail initialize-user job when Dashboard app is already configured #64

### Updates

* Update dashboard-backend to v0.2.6

## [1.0.3]

### Features

* Update frontend to 0.2.5

## [1.0.2]

### Features

* Update backend to 0.2.5

### Bug fixes

* Check if app exists before inserting into DB on initialization #64

## [1.0.1]

### Bug fixes

* Resolve "App access role is not persistent" #63

## [1.0.0]

### Bug fixes

* Resolve "New dashboard user management blocks all apps for admin user" #61
