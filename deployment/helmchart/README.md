# What is this?

Installs the Stackspin Dashboard into a Kubernetes cluster.

## Installation

**NOTE:** Some values need to be overwritten during installation. See `values-local.yaml.example` for an example of
configurations you might want to do. These instructions assume you copy
`values-local.yaml.example` to `values-local.yaml` and edit it.

It is very important that you overwrite the following password and
secrets variables manually when you run helm install:

- `backend.secretKey`
- `backend.kratosUrl`

Add our helm repository[^1]:

```
helm repo add stackspin-dashboard https://open.greenhost.net/api/v4/projects/48/packages/helm/stable
```

Then install the chart:

```
helm install -f values-local.yaml my-dashboard stackspin-dashboard/dashboard
```

[^1]: If you want to test a version that is not on the `main` branch yet, use `https://open.greenhost.net/api/v4/projects/1/packages/helm/unstable`
