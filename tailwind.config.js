module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          50: '#F2FFFF',
          100: '#D6FDFF',
          200: '#B6F7FB',
          300: '#7AE5EA',
          400: '#55C6CC',
          500: '#39A9B1',
          600: '#24929C',
          700: '#157983',
          800: '#135D66',
          900: '#0F4F57',
          light: '#54C6CC',
          DEFAULT: '#54C6CC',
          dark: '#1E8290',
        },
        logo: {
          bluegray: '#3f607d',
          yellow: '#f5bd1c',
          lightviolet: '#a587bf',
          darkviolet: '#755d86',
          azure: '#3a97a3',
          lightazure: ' #b4e0e4',
        },
      },
    },
  },
  variants: {
    extend: {
      tableLayout: ['hover', 'focus'],
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
