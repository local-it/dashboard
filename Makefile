tag = "$$(git describe --tags)"

build:
	$(MAKE) -C backend build
	docker build -t dashboard .
	docker tag dashboard yksflip/dashboard:$(tag)
	docker push yksflip/dashboard:$(tag)

rm:
	docker stack rm ${STACK_NAME}

deploy: rm
	DOMAIN=${DOMAIN} docker stack deploy --resolve-image always --compose-file compose.yml ${STACK_NAME}

update:
	docker pull yksflip/dashboard:latest
	docker service update dashboard_app --image yksflip/dashboard:latest --force

exec:
	docker exec -it $$(docker ps --format "{{ .Names }}" | grep dashboard) bash
