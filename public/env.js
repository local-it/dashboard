window.env = {
  REACT_APP_API_URL: 'http://localhost:5000/api/v1',
  REACT_APP_SSO_LOGOUT_URL: 'https://login.example.org/if/flow/default-invalidation-flow/'
};
