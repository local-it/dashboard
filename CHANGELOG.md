# Changelog

## Unreleased

- Fix login welcome message
- Clarify "set new password" button (#94)
- Show error messages when login fails, for example when a wrong password was
  entered (#96)

## [0.5.1]

- Fix bug of missing "Monitoring" app access when creating a new user.
- Add Velero to the list of installable apps, but hide it from the dashboard

## [0.5.0]

- Merge dashboard-backend repository into this repository, released as 0.5.0
