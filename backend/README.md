# Stackspin dashboard backend

Backend for the [Stackspin dashboard](https://open.greenhost.net/stackspin/dashboard)

## Login application

Apart from the dashboard backend this repository contains a flask application
that functions as the identity provider, login, consent and logout endpoints
for the OpenID Connect (OIDC) process.
The application relies on the following components:

 - **Hydra**: Hydra is an open source OIDC server.
   It means applications can connect to Hydra to start a session with a user.
   Hydra provides the application with the username
   and other roles/claims for the application.
   Hydra is developed by Ory and has security as one of their top priorities.

 - **Kratos**: This is Identity Manager
   and contains all the user profiles and secrets (passwords).
   Kratos is designed to work mostly between UI (browser) and kratos directly,
   over a public API endpoint.
   Authentication, form-validation, etc. are all handled by Kratos.
   Kratos only provides an API and not UI itself.
   Kratos provides an admin API as well,
   which is only used from the server-side flask app to create/delete users.

 - **MariaDB**: The login application, as well as Hydra and Kratos, need to store data.
   This is done in a MariaDB database server.
   There is one instance with three databases.
   As all databases are very small we do not foresee resource limitation problems.

If Hydra hits a new session/user, it has to know if this user has access.
To do so, the user has to login through a login application.
This application is developed by the Stackspin team (Greenhost)
and is part of this repository.
It is a Python Flask application
The application follows flows defined in Kratos,
and as such a lot of the interaction is done in the web-browser,
rather then server-side.
As a result,
the login application has a UI component which relies heavily on JavaScript.
As this is a relatively small application,
it is based on traditional Bootstrap + JQuery.

# Development

To develop the Dashboard,
you need a Stackspin cluster that is set up as a development environment.
Follow the instructions [in the dashboard-dev-overrides
repository](https://open.greenhost.net/stackspin/dashboard-dev-overrides#dashboard-dev-overrides)
in order to set up a development-capable cluster.
The end-points for the Dashboard,
as well as Kratos and Hydra, will point to `http://stackspin_proxy:8081` in that cluster.
As a result, you can run components using the `docker-compose` file in
this repository, and still log into Stackspin applications that run on the cluster.


## Setting up the local development environment

After this process is finished, the following will run locally:

- The [dashboard](https://open.greenhost.net/stackspin/dashboard)
- The
  [dashboard-backend](https://open.greenhost.net/stackspin/dashboard-backend)

The following will be available locally through a proxy and port-forwards:

- Hydra admin
- Kratos admin and public
- The MariaDB database connections

These need to be available locally, because Kratos wants to run on the same
domain as the front-end that serves the login interface.


### 1. Setup hosts file

The application will run on `http://stackspin_proxy`. Add the following line to
`/etc/hosts` to be able to access that from your browser:

```
127.0.0.1	stackspin_proxy
```

### 2. Kubernetes access

The script needs you to have access to the Kubernetes cluster that runs
Stackspin. Point the `KUBECONFIG` environment variable to a kubectl config. That
kubeconfig will be mounted inside docker containers, so also make sure your
Docker user can read it.

### 3. Run it all

Now, run this script that sets a few environment variables based on what is in
your cluster secrets, and starts `docker-compose` to start a reverse proxy as
well as the flask application in this repository.

```
./run_app.sh
```

### 4. Front-end developmenet

Start the [dashboard front-end app](https://open.greenhost.net/stackspin/dashboard/#yarn-start).
