#!/usr/bin/env bash

set -euo pipefail

export DATABASE_PASSWORD=$(kubectl get secret -n flux-system stackspin-single-sign-on-variables -o jsonpath --template '{.data.dashboard_database_password}' | base64 -d)
export DOMAIN=$(kubectl get secret -n flux-system stackspin-cluster-variables -o jsonpath --template '{.data.domain}' | base64 -d)
export HYDRA_CLIENT_SECRET=$(kubectl get secret -n flux-system stackspin-dashboard-local-oauth-variables -o jsonpath --template '{.data.client_secret}' | base64 -d)
export FLASK_SECRET_KEY=$(kubectl get secret -n flux-system stackspin-dashboard-variables -o jsonpath --template '{.data.backend_secret_key}' | base64 -d)


if [[ -z "$DATABASE_PASSWORD" ]]; then
    echo "Could not find database password in stackspin-single-sign-on-variables secret"
    exit 1
fi

if [[ -z "$DOMAIN" ]]; then
    echo "Could not find domain name in stackspin-cluster-variables secret"
    exit 1
fi

if [[ -z "$FLASK_SECRET_KEY" ]]; then
    echo "Could not find backend_secret_key in stackspin-dashboard-variables secret"
    exit 1
fi

if [[ -z "$HYDRA_CLIENT_SECRET" ]]; then
    echo "Could not find client_secret in stackspin-dashboard-local-oauth-variables secret"
    echo "make sure you add this secret following instructions in the dashboard-dev-overrides repository"
    exit 1
fi

KUBECTL_UID=${UID:-1001} KUBECTL_GID=${GID:-0} docker compose up
