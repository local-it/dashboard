
"""Generic classes used by different parts of the application"""

import urllib.request

# Instead of processing the redirect, we return, so the application
# can handle the redirect itself. This is needed to extract cookies
# etc.
class RedirectFilter(urllib.request.HTTPRedirectHandler):
    """Overrides the standard redirect handler so it does not automatically
    redirect. This allows for inspecting the return values before redirecting or
    override the redirect action"""

    # pylint: disable=too-many-arguments
    # This amount of arguments is expected by the HTTPRedirectHandler
    def redirect_request(self, req, fp, code, msg, headers, newurl):
        return None