
"""Custom exception handler to raise consistent exceptions, as different backend
raise different exceptions"""

class BackendError(Exception):
    """The backend error is raised when interacting with
    the backend fails or gives an unexpected result. The
    error contains a oneliner description of the problem"""
