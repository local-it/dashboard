from functools import wraps

from areas.roles.role_service import RoleService

from flask_jwt_extended import get_jwt, verify_jwt_in_request
from helpers import Unauthorized


def admin_required():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()

            user_id = claims["user_id"]
            is_admin = RoleService.is_user_admin(user_id)
            if is_admin:
                return fn(*args, **kwargs)
            else:
                raise Unauthorized("You need to have admin permissions.")

        return decorator

    return wrapper
