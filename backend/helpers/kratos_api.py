from logging import error
import requests

from config import *
from .error_handler import KratosError

class KratosApi:
    @staticmethod
    def __handleError(res):
        if res.status_code >= 400:
            message = res.json()["error"]["message"]
            raise KratosError(message, res.status_code)

    @staticmethod
    def get(url):
        try:
            res = requests.get("{}{}".format(KRATOS_ADMIN_URL, url))
            KratosApi.__handleError(res)
            return res
        except KratosError as err:
            raise err
        except:
            raise KratosError()

    @staticmethod
    def post(url, data):
        try:
            res = requests.post("{}{}".format(KRATOS_ADMIN_URL, url), json=data)
            KratosApi.__handleError(res)
            return res
        except KratosError as err:
            raise err
        except:
            raise KratosError()

    @staticmethod
    def put(url, data):
        try:
            res = requests.put("{}{}".format(KRATOS_ADMIN_URL, url), json=data)
            KratosApi.__handleError(res)
            return res
        except KratosError as err:
            raise err
        except:
            raise KratosError()

    @staticmethod
    def delete(url):
        try:
            res = requests.delete("{}{}".format(KRATOS_ADMIN_URL, url))
            KratosApi.__handleError(res)
            return res
        except KratosError as err:
            raise err
        except:
            raise KratosError()
