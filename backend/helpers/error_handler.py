from flask import jsonify
from jsonschema import ValidationError


class KratosError(Exception):
    pass

class AuthentikError(Exception):
    pass

class HydraError(Exception):
    pass


class BadRequest(Exception):
    pass

class Unauthorized(Exception):
    pass

def bad_request_error(e):
    message = e.args[0] if e.args else "Bad request to the server."
    return jsonify({"errorMessage": message}), 400


def validation_error(e):
    original_error = e.description
    return (
        jsonify({"errorMessage": "{} is not valid.".format(original_error.path[0])}),
        400,
    )


def kratos_error(e):
    message = "[KratosError] " + e.args[0] if e.args else "Failed to contact Kratos."
    status_code = e.args[1] if e.args else 500
    return jsonify({"errorMessage": message}), status_code


def hydra_error(e):
    message = "[HydraError] " + e.args[0] if e.args else "Failed to contact Hydra."
    status_code = e.args[1] if e.args else 500
    return jsonify({"errorMessage": message}), status_code


def global_error(e):
    message = str(e)
    return jsonify({"errorMessage": message}), 500

def unauthorized_error(e):
    message = str(e)
    return jsonify({"errorMessage": message}), 403
