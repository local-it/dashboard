from flask import request, session
from requests_oauthlib import OAuth2Session

from config import *
from helpers import HydraError


class LITOauth:
    @staticmethod
    def authorize():
        try:
            scopes = ["openid", "email", "profile", "goauthentik.io/api"]
            oauth = OAuth2Session(HYDRA_CLIENT_ID, redirect_uri=REDIRECT_URL, scope=scopes)
            authorization_url, state = oauth.authorization_url(
                HYDRA_AUTHORIZATION_BASE_URL
            )
            return authorization_url
        except Exception as err:
            raise HydraError(str(err), 500)

    @staticmethod
    def get_token(state, code):
        try:
            oauth = OAuth2Session(
                client_id=HYDRA_CLIENT_ID,
                redirect_uri=REDIRECT_URL,
                state=state,
            )
            token = oauth.fetch_token(
                token_url=TOKEN_URL,
                code=code,
                client_secret=HYDRA_CLIENT_SECRET,
                include_client_id=True,
            )

            session["oauth_token"] = token

            return token
        except Exception as err:
            raise HydraError(str(err), 500)

    @staticmethod
    def get_user_info():
        try:
            hydra = OAuth2Session(
                client_id=HYDRA_CLIENT_ID, token=session["oauth_token"]
            )
            user_info = hydra.get("{}/userinfo".format(HYDRA_PUBLIC_URL))

            return user_info.json()
        except Exception as err:
            raise HydraError(str(err), 500)
