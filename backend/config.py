import os


def env_file(key: str):
    file_env = os.environ.get(f"{key}_FILE")
    if file_env and os.path.exists(file_env):
        return open(file_env).read().rstrip('\n')
    return os.environ.get(key)


SECRET_KEY = env_file("SECRET_KEY")

HYDRA_CLIENT_ID = os.environ.get("HYDRA_CLIENT_ID")
HYDRA_CLIENT_SECRET = env_file("HYDRA_CLIENT_SECRET")

HYDRA_AUTHORIZATION_BASE_URL = os.environ.get("HYDRA_AUTHORIZATION_BASE_URL")
TOKEN_URL = os.environ.get("TOKEN_URL")
REDIRECT_URL = os.environ.get("REDIRECT_URL")

LOGIN_PANEL_URL = os.environ.get("LOGIN_PANEL_URL")

HYDRA_PUBLIC_URL = os.environ.get("HYDRA_PUBLIC_URL")
HYDRA_ADMIN_URL = os.environ.get("HYDRA_ADMIN_URL")
KRATOS_ADMIN_URL = os.environ.get("KRATOS_ADMIN_URL")
KRATOS_PUBLIC_URL = str(os.environ.get("KRATOS_PUBLIC_URL")) + "/"

SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Set this to "true" to load the config from a Kubernetes serviceaccount
# running in a Kubernetes pod. Set it to "false" to load the config from the
# `KUBECONFIG` environment variable.
LOAD_INCLUSTER_CONFIG = os.environ.get("LOAD_INCLUSTER_CONFIG") == "true"

AUTHENTIK_BASEURL = os.environ.get("AUTHENTIK_BASEURL")
