#!/bin/sh

set -eu
env
mkdir -p db
flask db upgrade
gunicorn app:app -b 0.0.0.0:5000 --workers "$(nproc)" --reload --capture-output --enable-stdio-inheritance --log-level DEBUG
