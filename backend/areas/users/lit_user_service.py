from flask import current_app
from helpers.authentik_api import AuthentikApi
from .user_service import UserService
from .models import User


class UserService(UserService):
    @staticmethod
    def get_users():
        user_list =  [User.from_authentik(u).to_dict() for u in AuthentikApi.get("/core/users")]
        return user_list

    @staticmethod
    def get_user(id):
        pass

    @staticmethod
    def post_user(data):
        pass

    @staticmethod
    def __start_recovery_flow(email):
        pass

    @staticmethod
    def put_user(id, user_editing_id, data):
        pass

    @staticmethod
    def delete_user(id):
        pass

    @staticmethod
    def post_multiple_users(data):
        pass

    @staticmethod
    def __insertAppRoleToUser(userId, userRes):
        pass
