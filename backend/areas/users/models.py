

class User:
    id = None
    uuid = None
    traits = None
    email = None
    name = None
    preferredUsername = None
    state = None

    def __init__(self):
        pass

    @staticmethod
    def from_authentik(authentik_user):
        u = User()
        u.id = authentik_user["pk"]
        u.uuid = authentik_user["uid"]
        u.name = authentik_user["name"]
        u.email = authentik_user["email"]
        u.traits = {
            "name": authentik_user["name"],
            "email": authentik_user["email"],
            "app_roles": []
        }
        u.preferredUsername = authentik_user["username"]
        u.state = "active" if authentik_user["is_active"] else ""
        return u

    def to_dict(self):
        return {
            "id": self.id,
            "traits": self.traits,
            "preferredUsername": self.preferredUsername,
            "state": self.state,
        }
