import re

schema = {
    "type": "object",
    "properties": {
        "email": {
            "type": "string",
            "description": "Email of the user",
            "pattern": r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
            "minLength": 1,
        },
        "app_roles": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Name of the app",
                        "minLenght": 1,
                    },
                    "role_id": {
                        "type": ["integer", "null"],
                        "description": "Role of the user",
                        "minimum": 1,
                    },
                },
                "required": ["name", "role_id"],
            },
        },
    },
    "required": ["email", "app_roles"],
}

schema_multiple = {
    "users": {
        "type": "array",
        "items": {
            "$ref": schema
        }
    }
}
