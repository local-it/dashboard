from flask import Blueprint

api_v1 = Blueprint("api_v1", __name__, url_prefix="/api/v1")


@api_v1.route("/")
@api_v1.route("/health")
def api_index():
    return "Stackspin API v1.0"
