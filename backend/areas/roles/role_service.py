from areas.apps.models import AppRole
from .models import Role


class RoleService:
    @staticmethod
    def get_roles():
        roles = Role.query.all()
        return [{"id": r.id, "name": r.name} for r in roles]

    @staticmethod
    def get_role_by_id(role_id):
        return Role.query.filter_by(id=role_id).first()

    @staticmethod
    def is_user_admin(userId):
        dashboard_role_id = AppRole.query.filter_by(user_id=userId, app_id=1).first().role_id
        return dashboard_role_id == 1