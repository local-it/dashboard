from sqlalchemy import Integer, String
from database import db


class Role(db.Model):
    NO_ACCESS_ROLE_ID = 3

    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(length=64))

    def __repr__(self):
        return f"Role {self.name}"
