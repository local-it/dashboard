from flask import jsonify, request
from flask_jwt_extended import jwt_required
from flask_cors import cross_origin

from areas import api_v1

from .role_service import RoleService


@api_v1.route("/roles", methods=["GET"])
@jwt_required()
@cross_origin()
def get_roles():
    roles = RoleService.get_roles()
    return jsonify(roles)
