from .models import App, AppRole

class AppsService:
    @staticmethod
    def get_all_apps():
        apps = App.query.all()
        return [app.to_dict() for app in apps]

    @staticmethod
    def get_app(slug):
        app = App.query.filter_by(slug=slug).first()
        return app.to_dict()

    @staticmethod
    def get_app_roles():
        app_roles = AppRole.query.all()
        return [{"user_id": app_role.user_id, "app_id": app_role.app_id, "role_id": app_role.role_id} for app_role in app_roles]
