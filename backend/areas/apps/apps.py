from flask import jsonify
from flask_jwt_extended import jwt_required
from flask_cors import cross_origin

from areas import api_v1
from .apps_service import AppsService


CONFIG_DATA = [
    {
        "id": "values.yml",
        "description": "Some user friendly description",
        "raw": "cronjob:\n  # Set curl to accept insecure connections when acme staging is used\n  curlInsecure: false",
        "fields": [
            {"name": "cronjob", "type": "string", "value": ""},
            {"name": "curlInsecure", "type": "boolean", "value": "false"}
        ]
    }
]

@api_v1.route('/apps', methods=['GET'])
@jwt_required()
@cross_origin()
def get_apps():
    """Return data about all apps"""
    apps = AppsService.get_all_apps()
    return jsonify(apps)


@api_v1.route('/apps/<string:slug>', methods=['GET'])
@jwt_required()
def get_app(slug):
    """Return data about a single app"""
    app = AppsService.get_app(slug)
    return jsonify(app)


@api_v1.route('/apps', methods=['POST'])
@jwt_required()
@cross_origin()
def post_app():
    """Unused function, returns bogus data for now"""
    return jsonify([]), 201


@api_v1.route('/apps/<string:slug>', methods=['PUT'])
@jwt_required()
@cross_origin()
def put_app(slug):
    """Unused function, returns bogus data for now"""
    return jsonify([])


@api_v1.route('/apps/<string:slug>/config', methods=['GET'])
@jwt_required()
@cross_origin()
def get_config(slug):
    """Returns bogus config data"""
    return jsonify(CONFIG_DATA)


@api_v1.route('/apps/<string:slug>/config', methods=['DELETE'])
@jwt_required()
@cross_origin()
def delete_config(slug):
    """Does nothing, then returns bogus config data"""
    return jsonify(CONFIG_DATA)
