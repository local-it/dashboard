from flask import Flask, jsonify
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from jsonschema.exceptions import ValidationError
from werkzeug.exceptions import BadRequest

# These imports are required
from areas import api_v1
from cliapp import cli
from web import web

from areas import users
from areas import apps
from areas import auth
from areas import roles
from cliapp import cliapp
from web import login

from database import db

from helpers import (
    BadRequest,
    KratosError,
    HydraError,
    Unauthorized,
    bad_request_error,
    validation_error,
    kratos_error,
    global_error,
    hydra_error,
    unauthorized_error,
)

from config import *
import logging

app = Flask(__name__)

app.config["SECRET_KEY"] = SECRET_KEY
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = SQLALCHEMY_TRACK_MODIFICATIONS

cors = CORS(app)
Migrate(app, db)
db.init_app(app)


app.logger.setLevel(logging.INFO)

app.register_blueprint(api_v1)
app.register_blueprint(web)
app.register_blueprint(cli)

# Error handlers
app.register_error_handler(Exception, global_error)
app.register_error_handler(BadRequest, bad_request_error)
app.register_error_handler(ValidationError, validation_error)
app.register_error_handler(KratosError, kratos_error)
app.register_error_handler(HydraError, hydra_error)
app.register_error_handler(Unauthorized, unauthorized_error)

jwt = JWTManager(app)

# When token is not valid or missing handler
@jwt.invalid_token_loader
@jwt.unauthorized_loader
@jwt.expired_token_loader
def expired_token_callback(*args):
    return jsonify({"errorMessage": "Unauthorized"}), 401


@app.route("/")
def index():
    return "Stackspin API v1.0"
