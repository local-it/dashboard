import os
from flask import Blueprint

web = Blueprint(
    "web",
    __name__,
    url_prefix="/web",
    static_folder="static",
    template_folder="templates",
)
